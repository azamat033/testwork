import Vue from "vue";
import VueRouter from 'vue-router';
import Start from "./views/Start";
import SingleCategory from "./views/SingleCategory";
import SingleTask from "./views/SingleTask";
Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',
    routes:[
        {path:'/', name:'Start', component: Start,},
        {path:'/singleCategory/:id', name:'SingleCategory', component: SingleCategory, props: true},
        {path:'/singleTask/:id', name:'SingleTask', component: SingleTask, props: true}
    ]
})
