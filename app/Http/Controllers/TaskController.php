<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $category_id = request('category_id');
        return Task::where('category_id', '=', $category_id)->get();
    }
    public function setStatus()
    {
        $request = request('params');
        $task_id = $request['task_id'];
        $status = $request['status'];
        $task = Task::find($task_id);
        $task->status = !$status;
        $res = $task->save();
        if ($res) {
            return 1;
        }
    }

    public function singleTask()
    {
        $task_id = request('task_id');
        return Task::find($task_id);
    }
}
